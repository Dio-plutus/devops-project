provider "aws" {
  region = "us-east-1"
}


#This is to save our terraform.tfstate file in s3 to keep our secret (passwords)
# terraform {
#   required_version = ">= 0.13.4"
#   backend "s3" {
#     bucket = "bucket_name"
#     key = "the location we want the state file to be stored on s3"
#     region = "us-east-1"
#   }
# }