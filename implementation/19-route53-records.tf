data "aws_route53_zone" "oldcowboyshop" {
  name         = "oldcowboyshop.com"
  private_zone = false
}


# create records for tooling
resource "aws_route53_record" "tooling" {
  zone_id = data.aws_route53_zone.oldcowboyshop.zone_id
  name    = "projecttooling.oldcowboyshop.com"
  type    = "A"

  alias {
    name                   = aws_lb.external-alb.dns_name
    zone_id                = aws_lb.external-alb.zone_id
    evaluate_target_health = true
  }
}


# create records for wordpress
resource "aws_route53_record" "wordpress" {
  zone_id = data.aws_route53_zone.oldcowboyshop.zone_id
  name    = "projectwordpress.oldcowboyshop.com"
  type    = "A"

  alias {
    name                   = aws_lb.external-alb.dns_name
    zone_id                = aws_lb.external-alb.zone_id
    evaluate_target_health = true
  }
}